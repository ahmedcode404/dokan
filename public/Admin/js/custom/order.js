$(document).ready(function () {
	
	$('.add_client_order').on('click' , function (e) {
	
		e.preventDefault();

		var name = $(this).data('name');
		var id = $(this).data('id');
		var price = $(this).data('price');

		$(this).removeClass('btn-info').addClass('btn-success disabled');

				//<input type="hidden" name="products[]" value="${id}"/>



		var html = 
			`<tr>
				<td>${name}</td>
				<td><input type="number" name="products[${id}][quantity]" data-price="${price}" value="1" min="1" class="form-control product_quantity" /></td>
				<td class="product_prices" >${price}</td>
				<td><button class="btn btn-danger remove-order" data-id="${id}"><i class="icon-trash"></i></button></td>
			</tr>`
		$('.order_list').append(html);	

		// of clauclate totlal
		calculate_total();

	}); 

		$('body').on('click' , '.disabled' , function (e) {
		e.preventDefault();
		

		}); 


		$('body').on('click' , '.remove-order' , function (e) {


		e.preventDefault();
		var id = $(this).data('id');

		$(this).closest('tr').remove();
		$('#product-' + id).removeClass('btn-success disabled').addClass('btn-info');
		
		// of calculate total
		calculate_total();

		});

		$('body').on('change' , '.product_quantity' , function (e) {

			e.preventDefault();
			var unitPrice = $(this).data('price')
			var proPrice = parseInt($(this).val());
			$(this).closest('tr').find('.product_prices').html( unitPrice * proPrice );

			calculate_total();
			}); // change

		$('.show_order').on('click' , function(e) {
			
			e.preventDefault();

			$('.loader').css('display' , 'flex');

			var url = $(this).data('url');
			var method = $(this).data('method');


			$.ajax({

				url: url,

				method: method,

				success: function (data) {

					$('.loader').css('display' , 'none');
					$('.unit_order').html(data);

				} // end success
			}); // end ajax

		}); // end click show order


		$(document).on('click' , '.print' , function () {
			$('.tag_print').printThis();
		})



}); // enf ready



function calculate_total() {

	var price = 0;

	$('.order_list .product_prices').each(function (index) {

		price += parseInt($(this).html());

	});

	$('.totalall').html(price);
}




