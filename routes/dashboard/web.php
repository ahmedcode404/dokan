<?php 

Route::get('Login' , 'DashboardController@login')->name('Login');
Route::post('Login/create' , 'DashboardController@createLogin')->name('Login.create');

Route::prefix('dashboard')->name('dashboard.')->middleware('role')->group(function () {



	Route::get('/' , 'DashboardController@index')->name('index')->middleware('role');

	// route categories

	Route::resource('Categories' , 'CategoryController')->except(['show']);

	// route product
	Route::resource('Products' , 'ProductController')->except(['show']);
	Route::delete('delete/{image}' , 'ProductController@delete_image');

	// route brand
	Route::resource('Brand' , 'BrandController')->except(['show']);


});