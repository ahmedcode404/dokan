<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'Market'] , function () {
	Route::get('/', 'SiteController@index')->name('index');
	Route::get('/products' , 'ShopController@index')->name('products');
	Route::get('/products/{id}' , 'ShopController@product')->name('product_details');
	Route::get('Category/Products', 'ShopController@getProduct')->name('Product');
	
	Route::get('/addToCart/{product}' , 'CartController@addToCart')->name('Cart.Add');
	Route::delete('/Remove/{product}' , 'CartController@destroy')->name('cart.remove')->middleware('session');
	Route::put('/update/{product}' , 'CartController@update')->name('cart.update')->middleware('session');
	Route::get('/Shopping' , 'CartController@showCart')->name('Shopping.Cart')->middleware('session');

	Route::get('/CheckOut' , 'OrderController@index')->name('Checkout.Cart')->middleware('session');
	Route::post('/Check' , 'OrderController@storeOrder')->name('Checkout')->middleware('session');
	Route::get('/Order' , 'OrderController@orders')->name('order.index')->middleware('order');
	Route::get('/Logout' , 'OrderController@logout')->name('logout');
	Route::post('/Login' , 'OrderController@login')->name('login');
	Route::post('/Mail' , 'SiteController@mail')->name('Mail');
	Route::get('/register/facebook/callback' , 'SiteController@mail')->name('register.facebook.callback');
	
});

