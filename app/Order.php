<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id' , 'name' , 'email' ,  'country' , 'street_address' , 'city' , 'postalCode' , 'phone_number' , 'comment' , 'cart' ];

    public function users()
    {
    	return $this->belongsTo(User::class);
    }
}
