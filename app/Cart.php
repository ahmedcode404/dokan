<?php

namespace App;

class Cart
{
    	public $items = [];
    	public $totalPrice;
    	public $totalQty;
    

    public function __construct($cart = null)
    {
    	if ($cart) {
    		$this->items = $cart->items;
    		$this->totalPrice = $cart->totalPrice;
    		$this->totalQty = $cart->totalQty;
    	} else {
    		$this->items = [];
    		$this->totalPrice = 0;
    		$this->totalQty = 0;	
    	}
    } // end function construct

    public function add($product)
    {
    	$item = [
            'id' => $product->id,
    		'title' => $product->title,
    		'price_sale' => $product->price_sale,
    		'image' => $product->image,
    		'qty' => 0
    	];

    	
    	if (!array_key_exists($product->id, $this->items)) {
    		// this is not found
    		$this->items[$product->id] = $item;
    		$this->totalQty +=1;
    		$this->totalPrice += $product->price_sale;
    	} else {
    		// this is found
    		$this->totalQty +=1;
    		$this->totalPrice += $product->price_sale;
    	}
    	// add qty 1
    	$this->items[$product->id]['qty']  += 1 ;
    } // end function add

    public function remove($id)
    {
        if (array_key_exists($id, $this->items)) {
            $this->totalQty -= $this->items[$id]['qty'];
            $this->totalPrice -= $this->items[$id]['qty'] * $this->items[$id]['price_sale'];
             unset($this->items[$id]);
        }

    }

    public function updateqty($id , $qty)
    {
            // delete qty
            $this->totalQty -= $this->items[$id]['qty'];
            $this->totalPrice -= $this->items[$id]['qty'] * $this->items[$id]['price_sale'];

            // add qty
            $this->items[$id]['qty'] = $qty;


            // add qty and price
            $this->totalQty += $qty;
            $this->totalPrice += $this->items[$id]['price_sale'] * $qty;





    }    
}   
	