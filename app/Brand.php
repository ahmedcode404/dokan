<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
   	protected $table = 'brands';
	protected $guarded = [];
    protected $fillable = ['id' , 'name'];

    public function products() {
    	return $this->hasMany(Product::class);
    }
}
