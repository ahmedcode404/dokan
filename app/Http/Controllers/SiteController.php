<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Alert;
use App\Mail\SendEmail;
use Illuminate\Support\Facades\Mail;
class SiteController extends Controller
{
    public function index() 
    {

    	$products = Product::limit(12)->get();
   
    	return view('Site.index' , compact('products'));
    }

    public function mail(Request $request)
    {
    	//$email = $request->get('email');
    	$data = $request->validate([
    		'email' => 'required'
    	]);

    	Mail::to('ahmedcode314@gmail.com')->send(new SendEmail($data));

    	return redirect()->back()->with('success' , 'Thanks Email is sent');
    }
}
