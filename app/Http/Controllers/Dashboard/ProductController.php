<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator,Redirect,Response,File;
use App\Product;
use App\Category;
use App\Brand;
use App\Image;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('Admin.products.index' , compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $brands = Brand::all();
        return view('Admin.products.create' , compact('categories' , 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'category_id' => 'required',
            'brand_id' => 'required',
            'title' => 'required',
            'desc' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg,gif',
            'more_image.*' => 'image|mimes:jpg,png,jpeg,gif',
            'price_sale' => 'required',
            'in_stock' => 'required',
            'discount' => 'required',
        ]);

        //$request_data = $request->all()

        if ($file = $request->file('image')) {
            
            $destenation_path = public_path('/Admin/images/products/');
            $name_image = uniqid() . $file->getClientOriginalName();
            $file->move($destenation_path , $name_image);


        }

        $product = new Product;
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->title = $request->title;
        $product->desc = $request->desc;
        $product->image = $name_image;
        $product->price_sale = $request->price_sale;
        $product->in_stock = $request->in_stock;
        $product->discount = $request->discount;
        $product->save();

        //$request_data = $request->all();

        if($files = $request->file('more_image')) {

            $destenation_path = public_path('/Admin/images/products/');

            foreach ($files as $file) {
                

                $name = uniqid() . $file->getClientOriginalName();


                $file->move($destenation_path , $name);

                $upload_image = new Image;

                $upload_image->product_id = $product->id;
                $upload_image->image_path = $name;
                $upload_image->save();


            } // end foreach

        } // end if 

        

        return redirect()->route('dashboard.Products.index')->with('success','Added Successfuly');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($product)
    {

        $product = Product::find($product);
        $categories = Category::all();
        $brands = Brand::all();
        return view('Admin.products.edit' , compact('product' , 'categories' , 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product , Image $image)
    {

        $request->validate([
            'category_id' => 'required',
            'brand_id' => 'required',
            'title' => 'required',
            'desc' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg,gif',
            'more_image.*' => 'image|mimes:jpg,png,jpeg,gif',
            'price_sale' => 'required',
            'in_stock' => 'required',
            'discount' => 'required',
        ]);


        $product = Product::find($product);

        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->title = $request->title;
        $product->desc = $request->desc;
        $product->price_sale = $request->price_sale;
        $product->in_stock = $request->in_stock;
        $product->discount = $request->discount;

        if ($file = $request->file('image')) {
            
            $destenation_path = public_path('/Admin/images/products/');
            $name_images = uniqid() . $file->getClientOriginalName();
            $file->move($destenation_path , $name_images);
            $product->image = $name_images;

        }


        $product->update();


        if($files = $request->file('more_image')) {

            $destenation_path = public_path('/Admin/images/products/');

            foreach ($files as $file_more) {
                
                $image = new Image;

                $name = uniqid() . $file_more->getClientOriginalName();

                $file_more->move($destenation_path , $name);

                $image->product_id = $product->id;
                $image->image_path = $name;
                
                $image->save();


            } // end foreach

        } // end if 

        

        return redirect()->route('dashboard.Products.index')->with('success','Added Successfuly');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($product)
    {
        $product = Product::find($product);
        $product->delete();
        return redirect()->route('dashboard.Products.index')->with('success','Deleted Successfuly');
    }

     public function delete_image($image)
    {
        $image = Image::find($image);
        $image->delete();
        return redirect()->back()->with('success','Deleted Successfuly');

    }

    
}
