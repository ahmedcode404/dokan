<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index() 
    {
    	return view('Admin.welcome');
    }

    public function login() 
    {
    	return view('Admin.login');
    }

    public function createLogin(Request $request) 
    {
    	$this->validate(request() , [
    		'email' => 'required',
    		'password' => 'required',
    	]);


    	if (auth()->attempt(request(['email' , 'password'])) == false) {
    		return redirect()->back()->with('error' , 'The Email Or Password IsCorrect');
    	}


    	return redirect('dashboard')->with('success' , 'Login Is Successfuly');
    }         
}
