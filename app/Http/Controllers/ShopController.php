<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Category;
use App\Product;
use App\Brand;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , Category $category)
    {
        $categories = Category::all();
        $brands = Brand::all();

        $products = Product::where(function($query) {
            $brandss = Input::has('brands') ? Input::get('brands') : [];

            // search brands
            if (isset($brandss)) {
                foreach ($brandss as $brand) {
                    $query->where('brand_id' , $brand);
                }
            }

        })->when($request->search , function ($q) use ($request) {
            $q->where( 'title' , 'like' , '%'  . $request->search .  '%');
        })->paginate(6);

        //$products->withPath('customize/url');        

        return view('Site.products' , compact('categories' , 'products' , 'brands'));
    }


    public function getProduct(Request $request)
    {
        $cat_id = $request->cat_id;

         $products = Product::where('category_id' , $cat_id)->get();

        return view('Site.productsPage' , compact('products'));
    }  


    public function product($id)
    {
        $product = Product::find($id);
        //dd($product);
        return view('Site.product_details' , compact('product'));
    }

    public function searchCat($option)
    {
        $cat=Model::where('option',$option)->get();
        return response()->json(['categoryData'=>$cat],200);
    }

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
