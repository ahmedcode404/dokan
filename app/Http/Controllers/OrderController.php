<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\User;
use App\Order;
use Hash;
use Auth;
class OrderController extends Controller
{

    public function index()
    {
    	if (session()->has('cart')) {
    		$cart = new Cart(session()->get('cart'));
    	}else {
    		$cart = null;
    	}
        return view('Site.checkout' , compact('cart'));

    }  // end function index

    public function storeOrder(Request $request)
    {
    	if (isset($request->customCheck2)) {
    		
    		$data = $request->validate([
    			'name' => 'required',
    			'email' => 'required',
    			'password' => 'required',
    			'confirm_password' => 'required|same:password',
    		]);
    		// create user and order
    		$user = User::create([
    			'name' => $request->get('name'),
    			'email' => $request->get('email'),
    			'password' => Hash::make($request->get('password')),
    		]);

    		auth()->login($user);

    		$request->validate([
    			'country' => 'required',
    			'street_address' => 'required',
    			'city' => 'required',
    			'postalCode' => 'required',
    			'phone_number' => 'required',
    			'comment' => 'required',
    			//'customCheck3' => 'required',
    		]);
    		//create order
    		$order = Order::create([
    			'user_id' => auth()->user()->id,
    			'country' => $request->get('country'),
    			'street_address' => $request->get('street_address'),
    			'city' => $request->get('city'),
    			'postalCode' => $request->get('postalCode'),
    			'phone_number' => $request->get('phone_number'),
    			'comment' => $request->get('comment'),
    			'cart' => serialize(session()->get('cart')),
    		]);

            session()->forget('cart');

    		return redirect()->route('products')->with('success' , 'CheckOut Is Successfuly');

    	} // end if request user


            // login user go creat order only
    		if (Auth::check()) {

	    		$request->validate([
	    			'country' => 'required',
	    			'street_address' => 'required',
	    			'city' => 'required',
	    			'postalCode' => 'required',
	    			'phone_number' => 'required',
	    			'comment' => 'required',
	    			//'customCheck3' => 'required',
	    		]);

	    		$order = Order::create([
	    			'user_id' => Auth::user()->id,
	    			'country' => $request->get('country'),
	    			'street_address' => $request->get('street_address'),
	    			'city' => $request->get('city'),
	    			'postalCode' => $request->get('postalCode'),
	    			'phone_number' => $request->get('phone_number'),
	    			'comment' => $request->get('comment'),
	    			'cart' => serialize(session()->get('cart')),
	    		]);

                session()->forget('cart');

                return redirect()->route('products')->with('success' , 'CheckOut Is Successfuly');


    		} // enf if check

            // create order only without create user
            $request->validate([
                'name' => 'required',
                'email' => 'required',
                'password' => 'required',
                'confirm_password' => 'required|same:password',                
                'country' => 'required',
                'street_address' => 'required',
                'city' => 'required',
                'postalCode' => 'required',
                'phone_number' => 'required',
                'comment' => 'required',
                //'customCheck3' => 'required',
            ]);
            //create order
            $order = Order::create([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('country')),                
                'country' => $request->get('country'),
                'street_address' => $request->get('street_address'),
                'city' => $request->get('city'),
                'postalCode' => $request->get('postalCode'),
                'phone_number' => $request->get('phone_number'),
                'comment' => $request->get('comment'),
                'cart' => serialize(session()->get('cart')),
            ]);

            session()->forget('cart');

            return redirect()->route('products')->with('success' , 'CheckOut Is Successfuly');



    } // end function store order

    public function orders()
    {
    	$orders = auth()->user()->orders;

    	$carts = $orders->transform(function($cart , $key ) {
    		return unserialize($cart->cart);
    	});
        return view('Site.orders' , compact('carts'));
    } // end function orders

    public function login(Request $request)
    {

		$this->validate(request() , [
			'email' => 'required',
			'password' => 'required',

		]); 


        if (auth()->attempt(request(['email', 'password'])) == false) {
            return redirect()->back()->with('error' , 'The Email Or Password Is InCorrect');         	
    	}

        return redirect()->route('index')->with('success' , 'The Login Is Successfuly');

    } // end function login

    public function logout()
    {

        auth()->logout();
    	
        session()->forget('cart');

        return redirect()->route('index');

    } // end function logout
}
