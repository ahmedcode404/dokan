<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\product;
use App\User;
use Alert;
use Auth;
class CartController extends Controller
{
    public function addToCart(product $product) {

        if (session('success')) {
            Alert::toast(session('success'),'success');
       }

    	if (session()->has('cart')) {
    		$cart = new Cart(session()->get('cart'));
    	} else {
    		$cart = new Cart();
    	}

    	 $cart->add($product);
    	 //dd($cart);
    	 session()->put('cart' , $cart);

    	 return redirect()->back()->with('success' , 'Add To Cart Success');
    }

    public function showCart(product $product) {
    	if (session()->has('cart')) {
    		$cart = new Cart(session()->get('cart'));
    	} else {
    		$cart = null;
    	}

    	 return view('Site.cart' , compact('cart'));
    }

    public function destroy(product $product)
    {
        $cart = new Cart(session()->get('cart'));
        $cart->remove($product->id);

        if ($cart->totalQty <= 0) {
            session()->forget('cart');
        } else{
            session()->put('cart' , $cart);
        }

        return redirect()->back()->with('success' , 'Remove Cart Success');

    }

    public function update(Request $request , product $product)
    {

        $request->validate([
            'qty' => 'required|numeric'
        ]);


        $cart = new Cart(session()->get('cart'));
        $cart->updateqty($product->id , $request->qty);

        session()->put('cart' , $cart);

        return redirect()->back()->with('success' , 'Remove Cart Success');

    }

    public function getUser() 
    {
        $user = Auth::user();

        dd($user);
    }     
} 
	