<?php

namespace App\Http\Middleware;

use Closure;

class Order
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user() && \Auth::user()->order > 0) {
            return $next($request);
        }

        return redirect('Market');
    }
}
