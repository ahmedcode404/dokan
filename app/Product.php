<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
	protected $guarded = [];
    protected $fillable = [ 'id' , 'category_id' , 'brand_id' , 'title' , 'desc' , 'image' , 'more_image' , 'in_stock' , 'price_sale' , 'discount'];

    public function images() {
    	return $this->hasMany(Image::class);
    }

    public function category() {
    	return $this->belongsTo(Category::class);
    }

    public function brand() {
    	return $this->belongsTo(Brand::class);
    }
}
