<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

	protected $table = 'images';
	protected $guarded = [];
    protected $fillable = [ 'id' , 'product_id' , 'image_path'];

    public function product() {
    	return $this->belongsTo(Product::class);
    }
}
