<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'category_id' => function() {
        	return \App\Category::all()->random();
         },
        'brand_id' => function () {
        	return \App\Brand::all()->random();
        },
        'title' => $faker->word,
        'desc' => $faker->paragraph,
        'in_stock' => $faker->numberBetween(2000 , 3000),
        'price_sale' => $faker->numberBetween(100 , 500),
        'discount' => $faker->numberBetween(10 , 30)
    ];
});
