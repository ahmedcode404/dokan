<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left"><img src="" class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        
                        <span class="media-heading text-semibold">
                            
                        </span>
                        <div class="text-size-mini text-muted">

                        
                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <br><br>
                <ul class="navigation navigation-main navigation-accordion">
                    <li >
                        <a href="{{ route('dashboard.index') }}" ><i class="icon-home"></i> <span>{{ trans('app.home') }}</span></a>
                     
                    </li>
                    
                    
                    <li >
                        <a href="#" ><i class="icon-info3"></i> <span>{{ trans('app.Categories') }}</span></a>
                        <ul>
                            <li><a href="{{ route('dashboard.Categories.index') }}">{{ trans('app.Categories') }}</a></li>
                        </ul>
                    </li>

                     <li >
                        <a href="#" ><i class="icon-info3"></i> <span>{{ trans('app.Products') }}</span></a>
                        <ul>
                            <li><a href="{{ route('dashboard.Products.index') }}">{{ trans('app.Products') }}</a></li>
                        </ul>
                    </li>

                     <li >
                        <a href="#" ><i class="icon-info3"></i> <span>{{ trans('app.Brand') }}</span></a>
                        <ul>
                            <li><a href="{{ route('dashboard.Brand.index') }}">{{ trans('app.Brand') }}</a></li>
                        </ul>
                    </li>
                    
                    


                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->
