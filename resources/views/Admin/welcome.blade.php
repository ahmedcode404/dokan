@extends('Admin.layouts.app')

@section('title')
    {{ trans('app.home') }}
@endsection

@section('header')

@endsection

@section('content')

    <div class="home" style="margin: 20px;">


        <div class="col-lg-4">
            <!-- Members online -->
            <div class="panel bg-teal-400"  style="padding: 20px 0px;background-color: #9a8160;border: 1px solid #9a8160;">
                <div class="panel-body">
                    <div class="heading-elements visible-lg visible-md visible-xs visible-sm">
                        <span class="heading-text badge bg-teal-800" style="font-size:20px;"></span>
                    </div>
    
                    <h3 class="no-margin"><a href="" style="color:#fff;">{{ trans('app.category') }}</a></h3>
                    
                    <div class="text-muted" style="text-align:center;margin-top: 30px;">
                        <a href="" style="color: #fff;border: 1px solid #fff;padding: 3px 10px;border-radius: 5px;"><i class="icon-arrow-right5"></i> View</a>
                    </div>
                </div>
            </div>
            <!-- /members online -->
        </div>

        
        <div class="col-lg-4">
            <!-- Members online -->
            <div class="panel bg-teal-400"  style="padding: 20px 0px;background-color: royalblue;border: 1px solid royalblue;">
                <div class="panel-body">
                    <div class="heading-elements visible-lg visible-md visible-xs visible-sm">
                        <span class="heading-text badge bg-teal-800" style="font-size:20px;"></span>
                    </div>
    
                    <h3 class="no-margin"><a href="" style="color:#fff;">{{ trans('app.product') }}</a></h3>
                    
                    <div class="text-muted" style="text-align:center;margin-top: 30px;">
                        <a href="" style="color: #fff;border: 1px solid #fff;padding: 3px 10px;border-radius: 5px;"><i class="icon-arrow-right5"></i> View</a>
                    </div>
                </div>
            </div>
            <!-- /members online -->
        </div>
   

   
        <div class="col-lg-4">
            <!-- Members online -->
            <div class="panel bg-teal-400"  style="padding: 20px 0px;background-color: purple;border: 1px solid purple;">
                <div class="panel-body">
                    <div class="heading-elements visible-lg visible-md visible-xs visible-sm">
                        <span class="heading-text badge bg-teal-800" style="font-size:20px;"></span>
                    </div>
    
                    <h3 class="no-margin"><a href="" style="color:#fff;">{{ trans('app.clients') }}</a></h3>
                    
                    <div class="text-muted" style="text-align:center;margin-top: 30px;">
                        <a href="" style="color: #fff;border: 1px solid #fff;padding: 3px 10px;border-radius: 5px;"><i class="icon-arrow-right5"></i> View</a>
                    </div>
                </div>
            </div>
            <!-- /members online -->
        </div>


        <!-- morris -->




   
        <!-- end morris -->

   
    </div>



@endsection

@section('footer')

    @include('Admin/layouts/message')

    <script type="text/javascript">

    
    </script>

@endsection
