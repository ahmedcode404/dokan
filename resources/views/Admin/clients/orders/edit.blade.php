@extends('Admin.layouts.app')

@section('title')
    {{ trans('app.name_site') }} | {{ trans('app.edit') }} {{ trans('app.orders') }}
@endsection

@section('header')

@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{ trans('app.home') }}</span> - {{ trans('app.orders') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="icon-home2 position-left"></i> {{ trans('app.home') }} </a></li>
                <li class=""><a href="{{ route('dashboard.orders.index') }}"> {{ trans('app.orders') }} </a></li>
                <li class="active">{{ trans('app.Edit') }}</li>
            </ul>
        </div>
    </div>


    <!-- /page header -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title col-sm-6 pull-left">{{ trans('app.edit') }}</h5>
        </div>

        <br>

        @include('Admin.layouts.partials')

               <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">{{ trans('app.orders') }}</h3>
            </div>
            <!-- /.box-header -->
            @foreach ($categories as $index=>$category)
           <div class="box-body">

            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel panel">
                  <div class="panel-heading" style="background: #263238; color: #fff;">
                    <h5 class="panel-title">
                      <a data-toggle="collapse" data-parent="#{{ str_replace(' ' , '_' , $category->name) }}" href="#{{ str_replace(' ' , '_' , $category->name) }}">
                        {{ $category->name }}
                      </a>
                    </h5>
                  </div>
                  <div id="{{ str_replace(' ' , '_' , $category->name) }}" class="panel-collapse">
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>{{ trans('app.Id') }}</th>
                                    <th>{{ trans('app.namepro') }}</th>
                                    <th>{{ trans('app.sale') }}</th>
                                    <th>{{ trans('app.total') }}</th>
                                    <th>{{ trans('app.Actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach ($category->products as $cat)
                                    <tr>
                                        <td>{{ $cat->id}}</td>
                                        <td>{{ $cat->name}}</td>
                                        <td>{{ $cat->sale_price}}</td>
                                        <td>{{ $cat->total }}</td>
                                        <td>
                                            <a href="" id="product-{{ $cat->id}}" data-id="{{ $cat->id}}" data-name="{{ $cat->name}}" data-total="{{ $cat->total}}" data-price="{{ $cat->sale_price}}" class="btn {{ in_array($cat->id , $order->products->pluck('id')->toArray()) ? 'btn-success disabled' : 'btn-info' }} add_client_order" >
                                                <i class="icon-add" style="color: #fff;"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
            @endforeach
          <!-- /.box -->
          
          </div>
          <!-- /.box -->

 
        </div>
        <!--/.col (right) -->


<!-- col 6  -->


        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">{{ trans('app.orders') }}</h3>
            </div>
            <!-- /.box-header -->
                <form action="{{ route('dashboard.clients.orders.update' , ['order' => $order->id , 'client' => $client->id ]) }}" method="post" class="form">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                    <div class="panel-body">
                        <table class="table table-hover ">
                            <thead>
                                <tr>
                                    <th>{{ trans('app.namepro') }}</th>
                                    <th>{{ trans('app.quantity') }}</th>
                                    <th>{{ trans('app.sale') }}</th>
                                    <th>{{ trans('app.Actions') }}</th>
                                </tr>
                            </thead>
                            <div class="lds-facebook loader"><div></div><div></div><div></div></div>
                            <tbody class="order_list">
                                @foreach ($order->products as $product)
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td><input type="number" name="products[{{ $product->id }}][quantity]" data-price="{{ $product->sale_price }}" value="{{ $product->pivot->quantity }}" min="1" class="form-control product_quantity" /></td>
                                    <td class="product_prices" >{{ $product->sale_price * $product->pivot->quantity }}</td>
                                    <td><button class="btn btn-danger remove-order" data-id="{{ $product->id }}"><i class="icon-trash"></i></button></td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                        <h5 style="display: inline-block;"> {{ trans('app.totalall') }}</h5> 
                        ( <span class="totalall">{{ $product->sale_price * $product->pivot->quantity }}</span> )
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary btn-block"> {{ trans('app.edit_order') }} <i class="icon-pencil"></i></button>
                    </div>
                </form>
            <!-- /.box -->
          </div>
          <!-- /.box -->

 
        </div>
        <!--/.col (right) -->


<!-- end col 6 -->


      </div>
      <!-- /.row -->
    </section>

    </div>
    <!-- /horizontal form modal -->

@endsection

@section('footer')

    @include('Admin/layouts/message')

    <script src="{{ url('Admin') }}/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="{{ url('Admin') }}/js/demo_pages/datatables_advanced.js"></script>

    <script src="{{ url('Admin') }}/js/custom/order.js"></script>

    <script src="{{ url('Admin') }}/js/plugins/ckeditor/ckeditor.js"></script>
    <!-- Theme JS files -->
	<script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/fileinput.min.js"></script>

    <script>
        $(document).ready(function(){
            CKEDITOR.config.language = "{{ app()->getLocale() }}"; 
        });

        function readURL(input) {
              
            }

            $(".image").change(function() {
              readURL(this);
              if (this.files && this.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                  $('.image_preview').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(this.files[0]); // convert to base64 string
              }
            });
    </script>
@endsection
