@extends('Admin.layouts.app')

@section('title')
    {{ trans('app.name_site') }} | {{ trans('app.edit') }} {{ trans('app.clients') }}
@endsection

@section('header')

@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{ trans('app.home') }}</span> - {{ trans('app.clients') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="icon-home2 position-left"></i> {{ trans('app.home') }} </a></li>
                <li class=""><a href="{{ route('dashboard.clients.index') }}"> {{ trans('app.clients') }} </a></li>
                <li class="active">{{ trans('app.Edit') }}</li>
            </ul>
        </div>
    </div>


    <!-- /page header -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title col-sm-6 pull-left">{{ trans('app.Edit') }}</h5>
        </div>

        <br>

            <form action="{{ route('dashboard.clients.update' , $client->id) }}" method="post" class="form-horizontal" enctype='multipart/form-data' style="margin:20px;">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                

                <div class="form-group">
                    <label class="control-label col-sm-3" for="name">{{ trans('app.name_clent') }}</label>
                    <div class="col-md-9">
                        <input id="name" type="text" value="{{ $client->name }}" class="form-control" name="name" placeholder="{{ trans('app.name_clent') }}">

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                @for($i = 0; $i < 2; $i++)

                <div class="form-group">
                    <label class="control-label col-sm-3" for="phone">{{ trans('app.phone') }}</label>
                    <div class="col-md-9">
                        <input id="phone" type="text" value="{{ $client->phone[$i] }}" class="form-control" name="phone[]" placeholder="{{ trans('app.phone') }}">

                        @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                @endfor

                <div class="form-group">
                    <label class="control-label col-sm-3" for="address">{{ trans('app.address') }}</label>
                    <div class="col-md-9">
                        <input id="name" type="text" value="{{ $client->address }}" class="form-control" name="address" placeholder="{{ trans('app.address') }}">

                        @if ($errors->has('address'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>


                <br>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary btn-lg"> {{ trans('app.edit') }} <i class="icon-pencil"></i></button>
                </div>
            </form>

    </div>
    <!-- /horizontal form modal -->


@endsection

@section('footer')

    @include('Admin/layouts/message')

    <script src="{{ url('Admin') }}/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="{{ url('Admin') }}/js/demo_pages/datatables_advanced.js"></script>

    <script src="{{ url('Admin') }}/js/plugins/ckeditor/ckeditor.js"></script>
    <!-- Theme JS files -->
	<script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/fileinput.min.js"></script>

    <script>
        $(document).ready(function(){
            CKEDITOR.config.language = "{{ app()->getLocale() }}"; 
        });

        function readURL(input) {
              
            }

            $(".image").change(function() {
              readURL(this);
              if (this.files && this.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                  $('.image_preview').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(this.files[0]); // convert to base64 string
              }
            });
    </script>
@endsection
