@extends('Admin.layouts.app')

@section('title')
    {{ trans('app.clients') }}
@endsection

@section('header')

@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{ trans('app.home') }}</span> - {{ trans('app.clients') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ route('dashboard.index') }}"><i class="icon-home2 position-left"></i> {{ trans('app.home') }} </a></li>
                <li class="active">{{ trans('app.clients') }}</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title col-sm-6 pull-left">{{ trans('app.clients') }} </h5>
    {{--        <form action="{{ route('dashboard.clients.index') }}" method="get" class="form-horizontal">
            <div class="form-group">
                    <div class="col-md-8">
                        <select name="category_id" class="form-control" id="category_id">
                            <option value="">----</option>
                        </select>
                    </div>
            </div>

            <div class="form-group">
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-primary"> {{ trans('app.search') }}</button>
                    </div>
            </div>
            </form> --}}           

            @if (auth()->user()->hasPermission('update_clients'))
            <a href="{{ route('dashboard.clients.create') }}" class="btn bg-teal-400 btn-labeled btn-rounded pull-right"><b><i class="icon-add"></i></b>{{ trans('app.Newclients') }}
            </a>
            @else
            <a href="#" class="btn bg-teal-400 btn-labeled btn-rounded pull-right disabled"><b><i class="icon-add"></i></b>{{ trans('app.Newclients') }}
            </a>
            @endif
            
        </div>

        
        <br>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <table class="table table-bordered table-hover datatable-highlight">
            <thead>
                <tr>
                    <th>{{ trans('app.Id') }}</th>
                    <th>{{ trans('app.name_clent') }}</th>
                    <th>{{ trans('app.phone') }}</th>
                    <th>{{ trans('app.address') }}</th>
                    <th>{{ trans('app.orders') }}</th>
                    <th>{{ trans('app.Actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clients as $index=>$client)
                    <tr>
                        <td>{{ $index + 1}}</td>
                        <td>{{ $client->name}}</td>
                        <td>( {{ is_array($client->phone) ? implode($client->phone , ' - ') : $client->phone }} )</td>
                        <td>{!! $client->address !!}</td>
                        <td>
                            @if (auth()->user()->hasPermission('update_orders'))
                            <a href="{{ route('dashboard.clients.orders.create' , $client->id) }}" class="label label-info label-rounded label-icon">
                                {{ trans('app.addorder') }}
                            </a>
                            @endif
                        </td>
                        <td>
                            <!-- <span class="label label-success label-rounded label-icon" data-toggle="modal" data-target="#show"><i class="icon-eye"></i></span> -->
                            @if (auth()->user()->hasPermission('update_clients'))
                            <a href="{{ route('dashboard.clients.edit', $client->id) }}" target="_blank">
                                <span class="label label-primary label-rounded label-icon"><i class=" icon-pencil"></i></span>
                            </a>
                            @else
                            <a class="label label-primary label-rounded label-icon disabled">
                                <i class=" icon-pencil"></i>
                            </a>
                            @endif
                            @if (auth()->user()->hasPermission('delete_clients'))
                            <form action="{{ route('dashboard.clients.destroy' , $client->id) }}" method="post" style="display: inline-block;">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button type="submit" class="label label-danger delete label-rounded label-icon">
                                    <i class="icon-trash"></i>
                                </button>
                            </form>
                            @else
                                <a class="label label-danger label-rounded label-icon disabled">
                                    <i class=" icon-trash"></i>
                                </a>
                            @endif
                           
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>


@endsection

@section('footer')

    @include('Admin/layouts/message')

    <script src="{{ url('Admin') }}/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="{{ url('Admin') }}/js/demo_pages/datatables_advanced.js"></script>
    
    

    <!-- Theme JS files -->
	<script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/fileinput.min.js"></script>

    <script>
        $(document).ready(function(){
            $(".delete").click(function(e){
                var that = $(this)

                e.preventDefault();

                var n = new Noty({
                    text: "@lang('app.confirm_delete')",
                    type: "warning",
                    killer: true,
                    buttons: [
                    Noty.button("@lang('app.yes')" , 'btn btn-success mr-2' , function () {
                        that.closest('form').submit();
                    }),

                    Noty.button("@lang('app.no')" , 'btn btn-danger mr-2' , function () {
                        n.close();
                    })
                    ]
                });
                n.show();
            });
        });
    </script>
@endsection
