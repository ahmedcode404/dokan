@extends('Admin.layouts.app')

@section('title')
    {{ trans('app.name_site') }} | {{ trans('app.orders') }}
@endsection

@section('header')

@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{ trans('app.home') }}</span> - {{ trans('app.orders') }}</h4> 
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ route('dashboard.index') }}"><i class="icon-home2 position-left"></i> {{ trans('app.home') }} </a></li>
                <li class="">{{ trans('app.orders') }}</li>
                {{--  @if(isset($find))
                    <li class="active">{{ trans('app.Edit') }}: $find->title_ar </li>
                @endif  --}}
            </ul>
        </div>
    </div>


    <!-- /page header -->
    <div class="panel panel-flat">



        @include('Admin.layouts.partials')

               <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">{{ trans('app.orders') }}</h3>
            </div>
            <!-- /.box-header -->
           <div class="box-body">

            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel panel">
                  <div id="" class="panel-collapse">
                    <div class="panel-body">
                        <table class="table table-bordered table-hover datatable-highlight">
                            <thead>
                                <tr>
                                    <th>{{ trans('app.name_clent') }}</th>
                                    <th>{{ trans('app.sale') }}</th>
                                    <th>{{ trans('app.show_product') }}</th>
                                    <th>{{ trans('app.date') }}</th>
                                    <th>{{ trans('app.Actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach ($orders as $order)
                                    <tr>
                                        <td>{{ $order->client->name}}</td>
                                        <td>{{ $order->total_price}}</td>
                                        <td>
                                            <a href="" data-url="{{ route('dashboard.orders.products' , $order->id) }}" data-method="get"  class="btn btn-info show_order" >
                                                {{ trans('app.show_product') }}
                                            </a></td>
                                        <td>{{ $order->created_at->toFormattedDateString() }}</td>
                                        <td>
                                        <!-- <span class="label label-success label-rounded label-icon" data-toggle="modal" data-target="#show"><i class="icon-eye"></i></span> -->
                                        
                                        @if (auth()->user()->hasPermission('update_orders'))
                                        <a href="{{ route('dashboard.clients.orders.edit' , [ 'client' => $order->client->id , 'order' => $order->id ]) }}" target="_blank">
                                            <span class="label label-primary label-rounded label-icon"><i class=" icon-pencil"></i></span>
                                        </a>
                                        @else
                                        <a class="label label-primary label-rounded label-icon disabled">
                                            <i class=" icon-pencil"></i>
                                        </a>
                                        @endif
                                        @if (auth()->user()->hasPermission('delete_orders'))
                                        <form action="{{ route('dashboard.orders.destroy' , $order->id) }}" method="post" style="display: inline-block;">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                            <button type="submit" class="label label-danger delete label-rounded label-icon">
                                                <i class="icon-trash"></i>
                                            </button>
                                        </form>
                                        @else
                                            <a class="label label-danger label-rounded label-icon disabled">
                                                <i class=" icon-trash"></i>
                                            </a>
                                        @endif
                                       
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
          </div>
          <!-- /.box -->

 
        </div>
        <!--/.col (right) -->


<!-- col 6  -->


        <div class="col-md-4">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">{{ trans('app.show_product') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="lds-facebook loader"><div></div><div></div><div></div></div>
                    <div class="panel-body unit_order">
                        
                    </div>
            <!-- /.box -->
          </div>
          <!-- /.box -->

 
        </div>
        <!--/.col (right) -->


<!-- end col 6 -->


      </div>
      <!-- /.row -->
    </section>

    </div>
    <!-- /horizontal form modal -->


@endsection

@section('footer')

    @include('Admin/layouts/message')
 
    <script src="{{ asset('Admin/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ url('Admin') }}/js/demo_pages/datatables_advanced.js"></script>

    <!-- <script src="{{ asset('') }}vendor/unisharp/laravel-ckeditor/ckeditor.js"></script> -->
    <!-- <script src="{{ url('Admin') }}/js/plugins/ckeditor/ckeditor.js"></script> -->

    <!-- Theme JS files -->
    <script src="{{ url('Admin') }}/js/custom/order.js"></script>
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
 
    <script>
 
        $(document).ready(function(){
            $(".delete").click(function(e){
                var that = $(this)

                e.preventDefault();

                var n = new Noty({
                    text: "@lang('app.confirm_delete')",
                    type: "warning",
                    killer: true,
                    buttons: [
                    Noty.button("@lang('app.yes')" , 'btn btn-success mr-2' , function () {
                        that.closest('form').submit();
                    }),

                    Noty.button("@lang('app.no')" , 'btn btn-danger mr-2' , function () {
                        n.close();
                    })
                    ]
                });
                n.show();
            });
        });
    </script>

@endsection

