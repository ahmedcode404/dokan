<div class="panel panel-heading tag_print">
<table class="table table-hover">
    <thead>
        <tr>
            <th>{{ trans('app.name_pro') }}</th>
            <th>{{ trans('app.quantity') }}</th>
            <th>{{ trans('app.sale') }}</th>
        </tr>
    </thead>

    <tbody class="order_list">
    	@foreach ($products as $product)
    	<tr>
    		<th>{{ $product->name }}</th>
    		<th>{{ $product->pivot->quantity }}</th>
    		<th>{{ $product->pivot->quantity * $product->sale_price }}</th>
    	</tr>
    	@endforeach
    </tbody>

</table>
<h5 style="display: inline-block;"> {{ trans('app.totalall') }}</h5> 
( <span class="totalall">{{ $order->total_price }}</span> )

</div>
<button type="submit" class="btn btn-primary btn-block print"> {{ trans('app.print') }} <i class="icon-add"></i></button>

 <script src="{{ url('Admin') }}/js/printThis.js"></script>
