@extends('Admin.layouts.app')

@section('title')
    {{ trans('app.name_site') }} | {{ trans('app.add') }} {{ trans('app.Categories') }}
@endsection

@section('header')

@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{ trans('app.home') }}</span> - {{ trans('app.Categories') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('admin/dashboard') }}"><i class="icon-home2 position-left"></i> {{ trans('app.home') }} </a></li>
                <li class="">{{ trans('app.Categories') }}</li>
                {{--  @if(isset($find))
                    <li class="active">{{ trans('app.Edit') }}: $find->title_ar </li>
                @endif  --}}
            </ul>
        </div>
    </div>


    <!-- /page header -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title col-sm-6 pull-left">{{ isset($find) ? trans('app.Edit') : trans('app.add') }}</h5>
        </div>

        <br>

        @include('Admin.layouts.partials')

            <form action="{{ route('dashboard.Products.store') }}" method="post" class="form-horizontal" enctype='multipart/form-data' style="margin:20px;">
                @csrf
                @method('POST')

                <div class="form-group">
                    <label class="control-label col-sm-3" for="category_id">{{ trans('app.Categories') }}</label>
                    <div class="col-md-9">
                        <select name="category_id" class="form-control">
                            <option value="" disabled="">{{ trans('app.Categories') }}</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="category_id">{{ trans('app.Brand') }}</label>
                    <div class="col-md-9">
                        <select name="brand_id" class="form-control">
                            <option value="" disabled="">{{ trans('app.Brand') }}</option>
                            @foreach($brands as $brand)
                            <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="name">{{ trans('app.title_pro') }}</label>
                    <div class="col-md-9">
                        <input id="name" type="text" class="form-control" value="{{ old('title') }}" name="title" placeholder="{{ trans('app.title_pro') }}">

                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="desc">{{ trans('app.desc') }}</label>
                    <div class="col-md-9">
                        <textarea id="desc" type="text" class="form-control" name="desc">{{ old('desc') }}</textarea>

                        @if ($errors->has('desc'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('desc') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="image">{{ trans('app.Image') }}</label>
                    <div class="col-md-9">
                        <input id="image" type="file" class="form-control image" value="{{ old('image') }}" name="image" placeholder="{{ trans('app.image') }}">

                        @if ($errors->has('image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <img src="{{ asset('Admin/images/products/df.jpg') }}" style="width: 100px" class="img-thumbnail image_preview" alt="">
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="more_image">{{ trans('app.Image_more') }}</label>
                    <div class="col-md-9">
                        <input id="more_image" type="file" class="form-control" value="{{ old('more_image') }}" name="more_image[]" multiple="" placeholder="{{ trans('app.Image_more') }}">

                        @if ($errors->has('image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="price_sale">{{ trans('app.price_sale') }}</label>
                    <div class="col-md-9">
                        <input id="price_sale" type="number" class="form-control" value="{{ old('price_sale') }}" name="price_sale" multiple="" placeholder="{{ trans('app.price_sale') }}">

                        @if ($errors->has('price_sale'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('price_sale') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="in_stock">{{ trans('app.in_stock') }}</label>
                    <div class="col-md-9">
                        <input id="in_stock" type="number" class="form-control" value="{{ old('in_stock') }}" name="in_stock" multiple="" placeholder="{{ trans('app.in_stock') }}">

                        @if ($errors->has('in_stock'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('in_stock') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="discount">{{ trans('app.discount') }}</label>
                    <div class="col-md-9">
                        <input id="discount" type="number" class="form-control" value="{{ old('discount') }}" name="discount" multiple="" placeholder="{{ trans('app.discount') }}">

                        @if ($errors->has('discount'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('discount') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>


                <br>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary btn-lg"> {{ trans('app.add') }} <i class="icon-add"></i></button>
                </div>
            </form>
            

    </div>
    <!-- /horizontal form modal -->


@endsection

@section('footer')

    @include('Admin/layouts/message')

    
    <script>
        $(document).ready(function(){
            CKEDITOR.config.language = "{{ app()->getLocale() }}"; 
        });
        function readURL(input) {
              
            }

            $(".image").change(function() {
              readURL(this);
              if (this.files && this.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                  $('.image_preview').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(this.files[0]); // convert to base64 string
              }
            });
    </script>

    <script src="{{ asset('') }}vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
    <script>
        CKEDITOR.replace( 'article-ckeditor2' );
    </script>

    <script src="{{ url('Admin') }}/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="{{ url('Admin') }}/js/demo_pages/datatables_advanced.js"></script>

    <!-- Theme JS files -->
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/fileinput.min.js"></script>



@endsection
