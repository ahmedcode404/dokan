@extends('Admin.layouts.app')

@section('title')
    {{ trans('app.name_site') }} | {{ trans('app.edit') }} {{ trans('app.Products') }}
@endsection

@section('header')

@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{ trans('app.home') }}</span> - {{ trans('app.Products') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="icon-home2 position-left"></i> {{ trans('app.home') }} </a></li>
                <li class=""><a href="{{ route('dashboard.Products.index') }}"> {{ trans('app.Products') }} </a></li>
                <li class="active">{{ trans('app.Edit') }}</li>
            </ul>
        </div>
    </div>


    <!-- /page header -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title col-sm-6 pull-left">{{ trans('app.Edit') }}</h5>
        </div>

        <br>

               <form action="{{ route('dashboard.Products.update' , $product->id) }}" method="post" class="form-horizontal" enctype='multipart/form-data' style="margin:20px;">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <label class="control-label col-sm-3" for="category_id">{{ trans('app.Categories') }}</label>
                    <div class="col-md-9">
                        <select name="category_id" class="form-control">
                            <option value="">{{ trans('app.Categories') }}</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('category_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('category_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="category_id">{{ trans('app.Brand') }}</label>
                    <div class="col-md-9">
                        <select name="brand_id" class="form-control">
                            <option value="">{{ trans('app.Brand') }}</option>
                            @foreach($brands as $brand)
                            <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('brand_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('brand_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="name">{{ trans('app.title_pro') }}</label>
                    <div class="col-md-9">
                        <input id="name" type="text" class="form-control" value="{{ old('title') ?? $product->title }}" name="title" placeholder="{{ trans('app.title') }}">

                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="desc">{{ trans('app.desc') }}</label>
                    <div class="col-md-9">
                        <textarea id="desc" type="text" class="form-control" name="desc">{{ old('desc') ?? $product->desc }}</textarea>

                        @if ($errors->has('desc'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('desc') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="image">{{ trans('app.Image') }}</label>
                    <div class="col-md-9">
                        <input id="image" type="file" class="form-control image" name="image" placeholder="{{ trans('app.image') }}">

                        @if ($errors->has('image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <img src="{{ asset('Admin/images/products/'. $product->image) }}" style="width: 100px" class="img-thumbnail image_preview" alt="">
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="more_image">{{ trans('app.Image_more') }}</label>
                    <div class="col-md-9">
                        <input id="more_image" type="file" class="form-control" name="more_image[]" multiple="" placeholder="{{ trans('app.Image_more') }}">

                        @if ($errors->has('image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-sm-3" for="price_sale">{{ trans('app.price_sale') }}</label>
                    <div class="col-md-9">
                        <input id="price_sale" type="number" class="form-control" value="{{ old('price_sale') ?? $product->price_sale }}" name="price_sale" placeholder="{{ trans('app.price_sale') }}">

                        @if ($errors->has('price_sale'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('price_sale') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="in_stock">{{ trans('app.in_stock') }}</label>
                    <div class="col-md-9">
                        <input id="in_stock" type="number" class="form-control" value="{{ old('in_stock') ?? $product->in_stock }}" name="in_stock" multiple="" placeholder="{{ trans('app.in_stock') }}">

                        @if ($errors->has('in_stock'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('in_stock') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="discount">{{ trans('app.discount') }}</label>
                    <div class="col-md-9">
                        <input id="discount" type="number" class="form-control" value="{{ old('discount') ?? $product->discount }}" name="discount" multiple="" placeholder="{{ trans('app.discount') }}">

                        @if ($errors->has('discount'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('discount') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>



                <br>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary btn-lg"> {{ trans('app.edit') }} <i class="icon-pencil"></i></button>
                </div>
            </form>

            <div class="form-group" 
                 style="display: inline-block; margin: 10px;">
                    @foreach($product->images as $image)
                        <img src="{{ asset('Admin/images/products/'. $image->image_path) }}" style="width: 100px" class="img-thumbnail" alt="" />
                        <form action="/dashboard/delete/{{ $image->id }} " method="post" style="display: inline-block; margin: 10px;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" tyle="display: inline-block; margin: 10px;">Delete</button>
                        </form>
                    @endforeach
                </div>

    </div>
    <!-- /horizontal form modal -->


@endsection

@section('footer')

    @include('Admin/layouts/message')

    <script src="{{ url('Admin') }}/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="{{ url('Admin') }}/js/demo_pages/datatables_advanced.js"></script>

    <!-- Theme JS files -->
	<script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/fileinput.min.js"></script>

    <script>
         $(document).ready(function(){

        });

        function readURL(input) {
              
            }

            $(".image").change(function() {
              readURL(this);
              if (this.files && this.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                  $('.image_preview').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(this.files[0]); // convert to base64 string
              }
            });


    </script>
@endsection
