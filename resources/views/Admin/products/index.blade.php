@extends('Admin.layouts.app')

@section('title')
    {{ trans('app.name_site') }} | {{ trans('app.Products') }}
@endsection

@section('header')

@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{ trans('app.home') }}</span> - {{ trans('app.Products') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ route('dashboard.index') }}"><i class="icon-home2 position-left"></i> {{ trans('app.home') }} </a></li>
                <li class="active">{{ trans('app.Products') }}</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title col-sm-6 pull-left">{{ trans('app.Products') }} </h5>
            <a href="{{ route('dashboard.Products.create') }}" class="btn bg-teal-400 btn-labeled btn-rounded pull-right"><b><i class="icon-add"></i></b>{{ trans('app.NewProducts') }}
            </a>
            
            
        </div>

        
        <br>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <table class="table table-bordered table-hover datatable-highlight">
            <thead>
                <tr>
                    <th>{{ trans('app.Id') }}</th>
                    <th>{{ trans('app.Categories') }}</th>
                    <th>{{ trans('app.Brand') }}</th>
                    <th>{{ trans('app.title_pro') }}</th>
                    <th>{{ trans('app.Image') }}</th>
                    <th>{{ trans('app.price_sale') }}</th>
                    <th>{{ trans('app.in_stock') }}</th>
                    <th>{{ trans('app.discount') }}</th>
                    <th>{{ trans('app.Actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $index=>$product)
                    <tr>
                        <td>{{ $index + 1}}</td>
                        <td>{{ $product->category->title}}</td>
                        <td>{{ $product->brand->name}}</td>
                        <td>{{ $product->title}}</td>
                        <td><img src="{{ asset('Admin/images/products/'. $product->image) }}" width="70px"></td>
                        <td>{{ $product->price_sale}}</td>
                        <td>{{ $product->in_stock}}</td>
                        <td>{{ $product->discount}}</td>
                        
                        <td>
                            <!-- <span class="label label-success label-rounded label-icon" data-toggle="modal" data-target="#show"><i class="icon-eye"></i></span> -->
                            <a href="{{ route('dashboard.Products.edit', $product->id) }}" target="_blank">
                                <span class="label label-primary label-rounded label-icon"><i class=" icon-pencil"></i></span>
                            </a>
                    
                            <form action="{{ route('dashboard.Products.destroy' , $product->id) }}" method="post" style="display: inline-block;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="label label-danger delete label-rounded label-icon">
                                    <i class="icon-trash"></i>
                                </button>
                            </form>
                            
                           
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>


@endsection

@section('footer')

    @include('Admin/layouts/message')

    <script src="{{ url('Admin') }}/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="{{ url('Admin') }}/js/demo_pages/datatables_advanced.js"></script>
    
    

    <!-- Theme JS files -->
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/fileinput.min.js"></script>

    <script>
        $(document).ready(function(){
            $(".delete").click(function(e){
                var that = $(this)

                e.preventDefault();

                var n = new Noty({
                    text: "@lang('app.confirm_delete')",
                    type: "warning",
                    killer: true,
                    buttons: [
                    Noty.button("@lang('app.yes')" , 'btn btn-success mr-2' , function () {
                        that.closest('form').submit();
                    }),

                    Noty.button("@lang('app.no')" , 'btn btn-danger mr-2' , function () {
                        n.close();
                    })
                    ]
                });
                n.show();
            });
        });
    </script>
@endsection
