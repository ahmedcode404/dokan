@extends('Admin.layouts.app')

@section('title')
    {{ trans('app.users') }}
@endsection

@section('header')

@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{ trans('app.home') }}</span> - {{ trans('app.users') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href=""><i class="icon-home2 position-left"></i> {{ trans('app.home') }} </a></li>
                <li class="active">{{ trans('app.users') }}</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title col-sm-6 pull-left">{{ trans('app.allusers') }} ( {{ $users->total() }} )</h5>
            @if (auth()->user()->hasPermission('delete_users'))
            <a href="{{ route('dashboard.users.create') }}" class="btn bg-teal-400 btn-labeled btn-rounded pull-right"><b><i class="icon-user-plus"></i></b>{{ trans('app.Newuser') }}
            </a>
            @else
            <a href="#" class="btn bg-teal-400 btn-labeled btn-rounded pull-right disabled"><b><i class="icon-user-plus"></i></b>{{ trans('app.Newuser') }}
            </a>
            @endif
        </div>

        
        <br>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <table class="table table-bordered table-hover datatable-highlight">
            <thead>
                <tr>
                    <th>{{ trans('app.Id') }}</th>
                    <th>{{ trans('app.first_name') }}</th>
                    <th>{{ trans('app.last_name') }}</th>
                    <th>{{ trans('app.image') }}</th>
                    <th>{{ trans('app.Actions') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $index=>$user)
                    <tr>
                        <td>{{ $index + 1}}</td>
                        <td>{{ $user->first_name}}</td>
                        <td>{{ $user->last_name}}</th>
                        <td><img src="{{ $user->image_path}}" class="img-thumbnail" style="width: 80px;" alt=""></th>
                        
                        <td>
                            <!-- <span class="label label-success label-rounded label-icon" data-toggle="modal" data-target="#show"><i class="icon-eye"></i></span> -->
                            @if (auth()->user()->hasPermission('update_users'))
                            <a href="{{ route('dashboard.users.edit', $user->id) }}" target="_blank">
                                <span class="label label-primary label-rounded label-icon"><i class=" icon-pencil"></i></span>
                            </a>
                            @else
                            <a class="label label-primary label-rounded label-icon disabled">
                                <i class=" icon-pencil"></i>
                            </a>
                            @endif
                            @if (auth()->user()->hasPermission('delete_users'))
                            <form action="{{ route('dashboard.users.destroy' , $user->id) }}" method="post" style="display: inline-block;">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button type="submit" class="label label-danger delete label-rounded label-icon">
                                    <i class="icon-trash"></i>
                                </button>
                            </form>
                            @else
                                <a class="label label-danger label-rounded label-icon disabled">
                                    <i class=" icon-trash"></i>
                                </a>
                            @endif
                           
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
    </div>


@endsection

@section('footer')

    @include('Admin/layouts/message')

    <script src="{{ url('Admin') }}/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="{{ url('Admin') }}/js/demo_pages/datatables_advanced.js"></script>

    <!-- Theme JS files -->
	<script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/fileinput.min.js"></script>

    <script>
        $(document).ready(function(){
            $(".delete").click(function(e){
                var that = $(this)

                e.preventDefault();

                var n = new Noty({
                    text: "@lang('app.confirm_delete')",
                    type: "warning",
                    killer: true,
                    buttons: [
                    Noty.button("@lang('app.yes')" , 'btn btn-success mr-2' , function () {
                        that.closest('form').submit();
                    }),

                    Noty.button("@lang('app.no')" , 'btn btn-danger mr-2' , function () {
                        n.close();
                    })
                    ]
                });
                n.show();
            });
        });
    </script>
@endsection
