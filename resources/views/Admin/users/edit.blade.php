@extends('Admin.layouts.app')

@section('title')
    {{ trans('app.users') }}
@endsection

@section('header')

@endsection

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{ trans('app.home') }}</span> - {{ trans('app.users') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="icon-home2 position-left"></i> {{ trans('app.home') }} </a></li>
                <li class="">{{ trans('app.users') }}</li>
                <li class="active">{{ trans('app.Edit') }}</li>
            </ul>
        </div>
    </div>


    <!-- /page header -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title col-sm-6 pull-left">{{ trans('app.Edit') }}</h5>
        </div>

        <br>

                <form action="{{ route('dashboard.users.update' , $user->id) }}" method="post" class="form-horizontal" enctype='multipart/form-data' style="margin:20px;">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="form-group">
                    <label class="control-label col-sm-3" for="name">{{ trans('app.first_name') }}</label>
                    <div class="col-md-9">
                        <input id="name" type="text" class="form-control" value="{{ $user->first_name }}" name="first_name" placeholder="{{ trans('app.first_name') }}">

                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="name">{{ trans('app.last_name') }}</label>
                    <div class="col-md-9">
                        <input id="name" type="text" class="form-control" value="{{ $user->last_name }}" name="last_name" placeholder="{{ trans('app.last_name') }}">

                        @if ($errors->has('last_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">{{ trans('app.Email') }}</label>
                    <div class="col-md-9">
                        <input id="email" type="email" class="form-control" value="{{ $user->email }}" name="email" placeholder="{{ trans('app.Email') }}">

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                 <div class="form-group">
                    <label class="control-label col-sm-3" for="image">{{ trans('app.image') }}</label>
                    <div class="col-md-9">
                        <input id="image" type="file" class="form-control image" name="image" placeholder="{{ trans('app.image') }}">

                        @if ($errors->has('image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <img src="{{ $user->image_path }}" style="width: 100px" class="img-thumbnail image_preview" alt="">
                </div>


                    <!-- permissions  -->

                    <div class="form-group">
                        <label class="control-label col-sm-3">{{ trans('app.premissioms') }}</label>
                    </div>    

                      <div class="nav-tabs-custom">
                        @php
                            $models = ['users' , 'categories' , 'products'];
                            $maps = ['create' , 'read' , 'update' , 'delete'];
                        @endphp
                        <ul class="nav nav-tabs">
                        @foreach($models as $index=>$model)
                          <li class="{{ $index == 0 ? 'active' : '' }}"><a href="#{{$model}}" data-toggle="tab">{{ trans('app.' . $model) }}</a></li>
                        @endforeach
                        </ul>

                        <div class="tab-content">
                            @foreach($models as $index=>$model)
                            <div class="tab-pane {{ $index == 0 ? 'active' : '' }}" id="{{$model}}">

                                @foreach ($maps as $map)

                                    <label><input type="checkbox" name="permissions[]" {{ $user->hasPermission($map . '_' . $model) ? 'checked' : '' }} value="{{ $map }}_{{ $model }}"> {{ trans('app.' . $map) }}</label>
                                @endforeach
                           </div>
                          <!-- /.tab-pane -->

                            @endforeach
                        </div>
                        <!-- /.tab-content -->
                      </div>
                      <!-- nav-tabs-custom -->

                   <!-- end permissions -->


                <br>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary btn-lg"> {{ trans('app.edit') }} <i class="icon-pencil"></i></button>
                </div>
            </form>

    </div>
    <!-- /horizontal form modal -->


@endsection

@section('footer')

    @include('Admin/layouts/message')

    <script src="{{ url('Admin') }}/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="{{ url('Admin') }}/js/demo_pages/datatables_advanced.js"></script>

    <!-- Theme JS files -->
	<script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
	<script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
    <script src="{{ url('Admin') }}/js/plugins/uploaders/fileinput/fileinput.min.js"></script>

    <script>
        $(document).ready(function(){

        });

        function readURL(input) {
              
            }

            $(".image").change(function() {
              readURL(this);
              if (this.files && this.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                  $('.image_preview').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(this.files[0]); // convert to base64 string
              }
            });
    </script>
@endsection
