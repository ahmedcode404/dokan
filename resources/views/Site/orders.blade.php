@extends('Site.layout.app')
@section('title')
Dokan | Orders
@endsection
@section('content')
        <div class="cart-table-area section-padding-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-12">
                        <div class="cart-title mt-50">
                            <h2>Orders</h2>
                        </div>
                        @foreach ($carts as $cart)
                        <div class="cart-table clearfix">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @foreach($cart->items as $item)
                                    <tr>
                                        <td class="cart_product_img">
                                            <a href="#"><img src="{{ url('Admin/images/products/' . $item['image']) }}" width="97px" alt="Product"></a>
                                        </td>
                                        <td class="cart_product_desc">
                                            <h5>{{ $item['title'] }}</h5>
                                        </td>
                                        <td class="price">
                                            <span>${{ $item['price_sale'] }}</span>
                                        </td>
                                        <td class="qty">
                                            <div class="qty-btn d-flex">
                                                <div class="quantity">
                                                    {{ $item['qty'] }}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                        <a class="btn amado-btn" style="color: #fff;">${{ $cart->totalPrice }}</a>
                        @endforeach  
                    </div>
                </div>
            </div>
        </div>

@endsection        