@extends('Site.layout.app')
@section('title')
Dokan | Check Out
@endsection
@section('content')
		<!-- checkout product -->
        <div class="cart-table-area section-padding-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="checkout_details_area mt-50 clearfix">

                            <div class="cart-title">
                                <h2>Checkout</h2>
                            </div>

                            <form action="{{ route('Checkout') }}" method="post">
                            	@csrf
                            	@method('POST')
                                <div class="row">
                                	@if(!Auth::check())
                                    <div class="col-12 mb-3">
                                        <input type="text" class="form-control" name="name" id="first_name" value="{{ old('name') }}" placeholder="User Name">                                      
                                    </div>
                                    <div class="col-12 mb-3">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{ old('email') }}">
                                    </div>
                                
                                    <div class="col-md-6 mb-3">
                                        <input type="password" class="form-control" name="password" id="password" value="{{ old('name') }}" placeholder="password">                                      
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <input type="password" class="form-control" name="confirm_password" id="confirm_password" value="{{ old('confirm_password') }}" placeholder="Confirm Password">                                      
                                    </div>
                                    @endif                                                                        
                                    <div class="col-12 mb-3">
                                        <select class="w-100" name="country" id="country">
                                        <option value="usa">United States</option>
                                        <option value="uk">United Kingdom</option>
                                        <option value="ger">Germany</option>
                                        <option value="fra">France</option>
                                        <option value="ind">India</option>
                                        <option value="aus">Australia</option>
                                        <option value="bra">Brazil</option>
                                        <option value="cana">Canada</option>
                                    </select>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <input type="text" class="form-control mb-3" name="street_address" id="street_address" placeholder="Street Address" value="{{ old('street_address') }}">
				                        @if ($errors->has('street_address'))
				                            <span class="invalid-feedback" role="alert">
				                                <strong>{{ $errors->first('street_address') }}</strong>
				                            </span>
				                        @endif                                        
                                    </div>
                                    <div class="col-12 mb-3">
                                        <input type="text" class="form-control" name="city" id="city" placeholder="Town" value="{{ old('city') }}">
				                        @if ($errors->has('city'))
				                            <span class="invalid-feedback" role="alert">
				                                <strong>{{ $errors->first('city') }}</strong>
				                            </span>
				                        @endif                                        
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <input type="text" class="form-control" name="postalCode" id="
                                        postalCode" placeholder="Postal Code" value="{{ old('postalCode') }}">
				                        @if ($errors->has('postalCode'))
				                            <span class="invalid-feedback" role="alert">
				                                <strong>{{ $errors->first('postalCode') }}</strong>
				                            </span>
				                        @endif                                        
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <input type="number" class="form-control" name="phone_number" id="phone_number" min="0" placeholder="Phone No" value="{{ old('phone_number') }}">
				                        @if ($errors->has('phone_number'))
				                            <span class="invalid-feedback" role="alert">
				                                <strong>{{ $errors->first('phone_number') }}</strong>
				                            </span>
				                        @endif                                        
                                    </div>
                                    <div class="col-12 mb-3">
                                        <textarea name="comment" class="form-control w-100" id="comment" cols="30" rows="10" placeholder="Leave a comment about your order">{{ old('comment') }}</textarea>
				                        @if ($errors->has('comment'))
				                            <span class="invalid-feedback" role="alert">
				                                <strong>{{ $errors->first('comment') }}</strong>
				                            </span>
				                        @endif                                        
                                    </div>

                                    <div class="col-12">
                                    	@if(!Auth::check())
                                        <p>If You Want To Create Acount Click Check Box</p>
                                        <div class="custom-control custom-checkbox d-block mb-2">
                                            <input type="checkbox" name="customCheck2" class="custom-control-input" id="customCheck2">
                                            <label class="custom-control-label" for="customCheck2">Create an accout</label>
                                        </div>
                                        @endif
			                            <div class="cart-btn mt-100">
			                                <button onclick="loading()" class="btn amado-btn w-100" type="submit">Checkout</button>
                                            <p style="display: none;">Please Wait ......</p>
			                            </div>
                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                    	@if($cart)
                        <div class="cart-summary">
                            <h5>Cart Total</h5>
                            <ul class="summary-table">
                            	
                                <li><span>subtotal:</span> <span>${{ $cart->totalPrice }}</span></li>
                                <li><span>delivery:</span> <span>Free</span></li>
                                <li><span>total:</span> <span>${{ $cart->totalPrice }}</span></li>
                            </ul>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
@endsection        

<script type="text/javascript">
    function loading() {
        loading.style.disblay = "block";
    }
</script>