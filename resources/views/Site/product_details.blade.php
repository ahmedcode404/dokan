@extends('Site.layout.app')
@section('title')
Dokan | Peoduct Details
@endsection
@section('content')
              
        <!-- Product Details Area Start -->
        <div class="single-product-area section-padding-100 clearfix">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mt-50">
                                <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                                <li class="breadcrumb-item">{{ $product->category->title }}</li>
                                <li class="breadcrumb-item active" aria-current="page">{{ $product->title }}</li>
                            </ol>
                        </nav>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-7">
                        <div class="single_product_thumb">

                            <div id="product_details_slider" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    @php 
                                        $i = 0;
                                    @endphp                                    
                                    @foreach($product->images as $img)
                                    <li class="{{ $i == 0 ? 'active' : '' }}" data-target="#product_details_slider" data-slide-to="{{$i}}" style="background-image: url('{{ asset('Admin/images/products/'. $img->image_path )}}');">
                                    @php 
                                        $i++;
                                    @endphp                                         
                                    </li>
                                    @endforeach
                                </ol>
                                <div class="carousel-inner">
                                    @php 
                                        $x = 0;
                                    @endphp
                                    @foreach($product->images as $img)
                                    <div class="carousel-item {{ $x == 0 ? 'active' : '' }}">
                                        <a class="gallery_img" href="{{ url('Admin/images/products/'. $img->image_path ) }}">
                                            <img class="d-block w-100" style="height: 500px;" src="{{ url('Admin/images/products/'.$img->image_path) }}" alt="">
                                        </a>
                                    </div>
                                    @php
                                        $x++;
                                    @endphp
                                    @endforeach

                                </div>
                            </div>
            
                        </div>
                    </div>
                    <div class="col-12 col-lg-5">
                        <div class="single_product_desc">
                            <!-- Product Meta Data -->
                            <div class="product-meta-data">
                                <div class="line"></div>
                                <p class="product-price">${{ $product->price_sale }}</p>
                                
                                    <h3>{{ $product->title }}</h3>
                                

                                <!-- Avaiable -->
                                <p class="avaibility"><i class="fa fa-circle"></i> In Stock</p>
                            </div>

                            <div class="short_overview my-5">
                                <p>{{ $product->desc }}</p>
                            </div>

                            <!-- Add to Cart Form -->
                                <a href="{{ route('Cart.Add' , $product->id) }}" onclick="message1()" name="addtocart" value="5" class="btn amado-btn">Add to cart</a>
                        </div>
                    <div class="row my-5">

                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox_32df"></div>
            
                    </div>

                    </div>

                </div>

            </div>

        </div>
        <!-- Product Details Area End -->

@endsection 


    <script type="text/javascript">

            function message1(id) {

                Swal.fire({
                  //position: 'top-end',
                  icon: 'success',
                  title: 'Add To Cart Successfull',
                  showConfirmButton: false,
                  timer: 1500
                })
            }

    </script>        