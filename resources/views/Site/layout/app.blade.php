<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>@yield('title')</title>

    <!-- Favicon  -->
    <link rel="icon" href="{{ url('Site') }}/img/core-img/favicon.ico">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="{{ url('Site') }}/css/core-style.css">
    <link rel="stylesheet" href="{{ url('Site') }}/style.css">

</head>

<body>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(session()->has('error'))
             <div class="alert alert-danger">
                <li>{{ session()->get('error') }}</li>
            </div>
            @endif
    <!-- Search Wrapper Area Start -->
    <div class="search-wrapper section-padding-100">
        <div class="search-close">
            <i class="fa fa-close" aria-hidden="true"></i>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="search-content">
                        <form action="#" method="get">
                            <input type="search" name="search" id="search" placeholder="Type your keyword...">
                            <button type="submit"><img src="{{ url('Site') }}/img/core-img/search.png" alt=""></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search Wrapper Area End -->

    <!-- Login Wrapper Area Start -->
    <div class="login-wrapper section-padding-100">
        <div class="login-close">
            <i class="fa fa-close" aria-hidden="true"></i>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="login-content">
                        <p><i class="fa fa-close" aria-hidden="true"></i> If you Do not Account please Go To Cart</p>
                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            <input type="text" name="email" placeholder="Type your Email..." >
                            <input type="password" name="password"  placeholder="Type your Password...">
                            <button type="submit"><img src="{{ url('Site') }}/img/core-img/search.png" alt=""></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Login Wrapper Area End -->


    <!-- ##### Main Content Wrapper Start ##### -->
    <div class="main-content-wrapper d-flex clearfix">

        <!-- Mobile Nav (max width 767px)-->
        <div class="mobile-nav">
            <!-- Navbar Brand -->
            <div class="amado-navbar-brand">
                <a href="index.html"><img src="{{ url('Site') }}/img/core-img/logo.jpg" alt=""></a>
            </div>
            <!-- Navbar Toggler -->
            <div class="amado-navbar-toggler">
                <span></span><span></span><span></span>
            </div>
        </div>

        <!-- Header Area Start -->
        <header class="header-area clearfix">
            <!-- Close Icon -->
            <div class="nav-close">
                <i class="fa fa-close" aria-hidden="true"></i>
            </div>
            <!-- Logo -->
            <div class="logo">
                <a href="{{ route('index') }}"><img src="{{ url('Site') }}/img/core-img/logo.jpg" alt=""></a>
            </div>
            <!-- Amado Nav -->

            <nav class="amado-nav">
                <ul>
                    <li class="{{ Request::path() == 'Market' ? 'active' : '' }}"><a href="{{ route('index') }}">Home</a></li>
                    <li  class="{{ Request::path() == 'Market/products' ? 'active' : '' }}"><a href="{{ route('products') }}">Shop</a></li>
                    @if(session()->get('cart'))
                    <li class="{{ Request::path() == 'Market/CheckOut' ? 'active' : '' }}"><a href="{{ route('Checkout.Cart') }}">Checkout</a></li>                    
                    @endif   
                    @if (Auth::check())
                    @if(auth()->user()->orders->count() > 0)   
                    <li class="{{ Request::path() == 'Market/Order' ? 'active' : '' }}"><a href="{{ route('order.index') }}">Orders</a></li>
                    @endif
                    <li><a href="{{ route('logout') }}">Logout</a></li>
                    @endif                    
                </ul>
            </nav>
            <!-- Button Group -->
            <div class="amado-btn-group mt-30 mb-100">
                <a href="#" class="btn amado-btn mb-15">%Discount%</a>
                <a href="#" class="btn amado-btn active">New this week</a>
            </div>
            <!-- Cart Menu -->
            <div class="cart-fav-search mb-100">
                @if(session()->get('cart'))
                <a href="{{ route('Shopping.Cart') }}" class="cart-nav"><img src="{{ url('Site') }}/img/core-img/cart.png" alt=""> Cart <span>({{ session()->has('cart') ? session()->get('cart')->totalQty : '0' }})</span></a>
                @endif
                <a href="#" class="search-nav"><img src="{{ url('Site') }}/img/core-img/search.png" alt=""> Search</a>
                @if (!Auth::check())
                <a href="#" class="login-nav"><img src="{{ url('Site') }}/img/core-img/search.png" alt=""> Login</a>
                @endif                
            </div>
    
        </header>
        <!-- Header Area End -->


        <!-- Product Catagories Area Start -->

        @yield('content')


        <!-- Product Catagories Area End -->
    </div>
    <!-- ##### Main Content Wrapper End ##### -->

    <!-- ##### Newsletter Area Start ##### -->
    <section class="newsletter-area section-padding-100-0">
        <div class="container">
            <div class="row align-items-center">
                <!-- Newsletter Text -->
                <div class="col-12 col-lg-6 col-xl-7">
                    <div class="newsletter-text mb-100">
                        <h2>Subscribe for a <span>25% Discount</span></h2>
                        <p>Dokan Company Work in Markrting Any product of clothes , watches and laptops this is product form best Brands of zara , versace , hp , dell and rado.</p>
                    </div>
                </div>
                <!-- Newsletter Form -->
                <div class="col-12 col-lg-6 col-xl-5">
                    <div class="newsletter-form mb-100">
                        <form action="{{ route('Mail') }}" method="post">
                            @csrf
                            <input type="email" name="email" class="nl-email" placeholder="Your E-mail">
                            <input type="submit" value="Subscribe">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Newsletter Area End ##### -->
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif  
    <!-- ##### Footer Area Start ##### -->
    <footer class="footer_area clearfix">
        <div class="container">
            <div class="row align-items-center">
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-4">
                    <div class="single_widget_area">
                        <!-- Logo -->
                        <div class="footer-logo mr-50">
                            <a href="{{ route('index') }}"><img src="{{ url('Site') }}/img/core-img/logo.jpg" alt=""></a>
                        </div>
                        <!-- Copywrite Text -->
                        <p class="copywrite"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This Programinig Made with <i class="fa fa-program" aria-hidden="true"></i> By <a href="https://www.facebook.com/ahmed.elbhery.311" target="_blank" style="color: rgb(7 178 199);"><b>Ahmed Code</b></a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-8">
                    <div class="single_widget_area">
                        <!-- Footer Menu -->
                        <div class="footer_menu">
                            <nav class="navbar navbar-expand-lg justify-content-end">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#footerNavContent" aria-controls="footerNavContent" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
                                <div class="collapse navbar-collapse" id="footerNavContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item {{ Request::path() == 'Market' ? 'active' : '' }}">
                                            <a class="nav-link" href="{{ route('index') }}">Home</a>
                                        </li>
                                        <li class="nav-item {{ Request::path() == 'Market/products' ? 'active' : '' }}">
                                            <a class="nav-link" href="{{ route('products') }}">Shop</a>
                                        </li>
                                        @if(session()->get('cart'))
                                        <li class="nav-item {{ Request::path() == 'Market/Shopping' ? 'active' : '' }}">
                                            <a class="nav-link" href="{{ route('Shopping.Cart') }}">Cart</a>
                                        </li>
                                        <li class="nav-item {{ request()->path() == 'Market/CheckOut' ? 'active' : '' }}">
                                            <a class="nav-link" href="{{ route('Checkout.Cart') }}">Checkout</a>
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area End ##### -->
    <!-- ##### jQuery (Necessary for All JavaScript Plugins) ##### -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{{ url('Site') }}/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="{{ url('Site') }}/js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="{{ url('Site') }}/js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="{{ url('Site') }}/js/plugins.js"></script>
    <!-- Active js -->
    <script src="{{ url('Site') }}/js/active.js"></script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f185929423cf156"></script>
@yield('footer')
</body>

</html>