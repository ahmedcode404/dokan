@extends('Site.layout.app')
@section('title')
Dokan | Cart
@endsection
@section('content')
		@if ($cart)
        <div class="cart-table-area section-padding-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="cart-title mt-50">
                            <h2>Shopping Cart</h2>
                        </div>

                        <div class="cart-table clearfix">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@foreach($cart->items as $item)
                                    <tr>

                                        <td class="cart_product_img">
                                            <a href="#"><img src="{{ url('Admin/images/products/' .$item['image']) }}" alt="Product"></a>
                                        </td>
                                        <td class="cart_product_desc">
                                            <h5>{{ $item['title'] }}{{ $item['id'] }}</h5>
                                        </td>
                                        <td class="price">
                                            <span>${{ $item['price_sale'] }}</span>
                                        </td>
                                        <td class="qty">
                                            <div class="qty-btn d-flex">
                                                <p>Qty</p>
                                                <div class="quantity">
                                                    <form action="{{ route('cart.update' , $item['id'] ) }}" method="post">
                                                        @csrf
                                                        @method('put')
                                                        <input type="number" name="qty" value="{{$item['qty']}}">
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="delete">
                                            <form action="{{ route('cart.remove' , $item['id'] ) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" style="min-width: 41px;" class="btn amado-btn w-100 btn-x2">Delete</button>
                                            </form>
                                        </td>                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="cart-summary">
                            <h5>Cart Total</h5>
                            <ul class="summary-table">
                                <li><span>subtotal:</span> <span>${{ $cart->totalPrice }}</span></li>
                                <li><span>delivery:</span> <span>Free</span></li>
                                <li><span>Total Quanttites:</span> <span>{{$cart->totalQty}}</span></li>
                                <li><span>total:</span> <span>${{ $cart->totalPrice }}</span></li>
                            </ul>
                            <div class="cart-btn mt-100">
                                <a href="{{ route('Checkout.Cart') }}" class="btn amado-btn w-100">Checkout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
        <p style="margin-top: 68px;color: #fbb710;">There Are No Items In Cart</p>
        @endif
@endsection        