@extends('Site.layout.app')
@section('title')
Dokan | Products
@endsection
@section('content')
<style type="text/css">
    .products-message {
position: relative;
display: none
}
.products-message .overlay {
position: fixed;
top: 173px;
left: 400px;
width: 40%;
height: 30%;
z-index: 100;
background-color:rgba(0,35,35,.6)
}
.active-message {
    margin-top: 114px;
    margin-right: 68px;
    margin-bottom: 114px;
    margin-left: 72px;   
}


</style>
        <div class="shop_sidebar_area">

            <!-- ##### Single Widget ##### -->
            <div class="widget catagory mb-50">
                <!-- Widget Title -->
                <h6 class="widget-title mb-30">Catagories</h6>

                <!--  Catagories  -->
                <div class="catagories-menu">
                    <ul>
                        <li><a href="{{ route('products') }}" >All</a></li>
                        @foreach($categories as $category)
                        <li><a style="cursor:pointer" onclick="getProduct({{$category->id}})" >{{ $category->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <!-- ##### Single Widget ##### -->
            <div class="widget brands mb-50">
                <!-- Widget Title -->
                <h6 class="widget-title mb-30">Brands</h6>

                <div class="widget-desc">
                    <!-- Single Form Check -->
                    <form method="get" action="{{ url('Market/products') }}">
                    @foreach($brands as $brand)
                    <div class="form-check">
                        <input class="typeahead form-check-input" type="checkbox" value="{{ $brand->id }}" name="brands[]" id="termsCheck{{ $brand->id }}"  onchange="this.form.submit()" >
                        <label class="form-check-label"  for="">{{ $brand->name }}</label>
                    </div>
                    @endforeach
                </form>
                    <!-- Single Form Check -->

                </div>
            </div>

        </div>

        <div class="amado_product_area section-padding-100">
            <div class="container-fluid">

                <div class="row" id="getProduct">
                    @if($products->count())

                    @foreach ($products as $product)

                    <!-- Single Product Area -->
                    <div class="col-12 col-sm-6 col-md-12 col-xl-6">
                        <div class="single-product-wrapper">
                            <!-- Product Image -->
                            <div class="product-img">
                                <img src="{{ url('Admin/images/products/' . $product->image) }}" style="height: 350px;" alt="">
                                <!-- Hover Thumb -->
                                <img class="hover-img" src="{{ url('Admin/images/products/product.jpg') }}" style="height: 350px;" alt="">
                            </div>

                            <!-- Product Description -->
                            <div class="product-description d-flex align-items-center justify-content-between">
                                <!-- Product Meta Data -->
                                <div class="product-meta-data">
                                    <div class="line"></div>
                                    <p class="product-price">${{ $product->price_sale }}</p>
                                    <a href="{{ route('product_details' , $product->id) }}">
                                        <h6>{{ $product->title }}</h6>
                                    </a>
                                </div>
                                <!-- Ratings & Cart -->
                                <div class="ratings-cart text-right">
                                    <div class="ratings">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                    <div class="cart">

                                        <a href="{{ route('Cart.Add' , $product->id) }}" data-toggle="tooltip" data-placement="left" title="Add to Cart"><img src="{{ url('Site/img/core-img/cart.png') }}" alt="" onclick="message()" ></a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endforeach

                    @else
                        <h4 style="text-align: center;margin-top: 59px;color: #07b2c7;">Sorry , Do Not Found Products.....</h4>
                    @endif                    

                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <!-- Pagination -->
                        <nav aria-label="navigation">
                            {{ $products->links('pagination-links') }}
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        

@endsection

@section('footer')
    <script src="{{ url('Site') }}/js/jquery/jquery-2.2.4.min.js"></script>
    <script type="text/javascript">
        function getProduct (id) {
            $.ajax({

                type: 'GET',

                dataType: 'html',

                url: '{{route('Product')}}',

                data: {cat_id: id},

                success: function(data){
                    $("#getProduct").html(data);
                }
            });
        }// end get products

            function message(id) {

                Swal.fire({
                  //position: 'top-end',
                  icon: 'success',
                  title: 'Add To Cart Successfull',
                  showConfirmButton: false,
                  timer: 1500
                })
                $(".products-message").show();
            }

            function hide() {
                $(".products-message").hide();
            }

                // $(document).ready(function(){

                //     $(".card").click(function(){

                //     $(".products-message").show();

                //     });

                //     $(".cont").click(function(){

                //     $(".products-message").hide();

                //     });

                // });
    </script> 
@endsection