<!-- Favicon  -->
<link rel="icon" href="{{ url('Site') }}/img/core-img/favicon.ico">

<!-- Core Style CSS -->
<link rel="stylesheet" href="{{ url('Site') }}/css/core-style.css">
<link rel="stylesheet" href="{{ url('Site') }}/style.css">
                
@foreach ($products as $product)
<!-- Single Product Area -->
<div class="col-12 col-sm-6 col-md-12 col-xl-6">
    <div class="single-product-wrapper">
        <!-- Product Image -->
        <div class="product-img">
            <img src="{{ url('Admin/images/products/' . $product->image) }}" style="height: 350px;" alt="">
            <!-- Hover Thumb -->
            <img class="hover-img" src="{{ url('Admin/images/products/product.jpg') }}" style="height: 350px;" alt="">
        </div>

        <!-- Product Description -->
        <div class="product-description d-flex align-items-center justify-content-between">
            <!-- Product Meta Data -->
            <div class="product-meta-data">
                <div class="line"></div>
                <p class="product-price">${{ $product->price_sale }}</p>
                <a href="{{ route('product_details' , $product->id) }}">
                    <h6>{{ $product->title }}</h6>
                </a>
            </div>
            <!-- Ratings & Cart -->
            <div class="ratings-cart text-right">
                <div class="ratings">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                </div>
                <div class="cart">
                    <a href="{{ route('Cart.Add' , $product->id) }}" data-toggle="tooltip" data-placement="left" title="Add to Cart"><img src="{{ url('Site/img/core-img/cart.png') }}" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
<!-- ##### jQuery (Necessary for All JavaScript Plugins) ##### -->
<script src="{{ url('Site') }}/js/jquery/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="{{ url('Site') }}/js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="{{ url('Site') }}/js/bootstrap.min.js"></script>
<!-- Plugins js -->
<script src="{{ url('Site') }}/js/plugins.js"></script>
<!-- Active js -->
<script src="{{ url('Site') }}/js/active.js"></script>
