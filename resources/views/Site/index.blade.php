@extends('Site.layout.app')
@section('title')
Dokan | Home 
@endsection
@section('content')

        <div class="products-catagories-area clearfix">
            <div class="amado-pro-catagory clearfix">

                <!-- Single Catagory -->
                @if($products->count())
                @foreach($products as $product)
                <div class="single-products-catagory clearfix">
                    <a href="{{ route('product_details' , $product->id) }}">
                        <img src="{{ url('Admin/images/products/' . $product->image) }}" width="100px" style="height: 380px;" alt="">
                        <!-- Hover Content -->
                        <div class="hover-content">
                            <div class="line"></div>
                            <p>From ${{$product->price_sale}}</p>
                            <h4>{{$product->title}}</h4>
                        </div>
                    </a>
                </div>
                @endforeach
                @else
                    <h4 style="text-align: center;margin-top: 59px;color: #fbb808;">Sorry , Do Not Found Products.....</h4>
                @endif
                <!-- end Single Catagory -->
            </div>
        </div>

@endsection