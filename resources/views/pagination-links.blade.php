
    <!-- Core Style CSS -->
    <link rel="stylesheet" href="{{ url('Site') }}/style.css">
@if ($paginator->hasPages())
<ul class="pagination justify-content-end mt-50">



        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item active"><a class="page-link" href="#">{{ $element }}</a></li>
            @endif


            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active"><a class="page-link" href="#">0{{ $page }}.</a></li>
                    @else
                    <li class="page-item"><a class="page-link" href="{{ $url }}">0{{ $page }}.</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach



    </ul>
@endif

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{{ url('Site') }}/js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <!-- Bootstrap js -->
    <script src="{{ url('Site') }}/js/bootstrap.min.js"></script>
